//
//  MovieTitleContainerView.swift
//  TopFilmes
//
//  Created by Leandro Romano on 21/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import UIKit

class MovieTitleContainerView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.clipsToBounds = true
        self.backgroundColor = .white
        layer.cornerRadius = 10.0
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }

}
