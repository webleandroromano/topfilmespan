//
//  MovieCollectionViewCell.swift
//  TopFilmes
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var ivCover: MovieBackgroundImageView!
    @IBOutlet weak var lbVoteAverage: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbGenres: UILabel!
    
    // MARK: - Properties
    
    var movieViewModel: MovieViewModel! {
        didSet {
            lbTitle.text = movieViewModel.title
            lbVoteAverage.text = movieViewModel.voteAverage
            ivCover.image = movieViewModel.cover
            lbGenres.text = movieViewModel.genres
        }
    }
    
}
