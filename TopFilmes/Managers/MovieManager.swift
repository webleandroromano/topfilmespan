//
//  MovieManager.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class MovieManager {
    
    // MARK: - Singleton
    
    static let shared = MovieManager()
    
    // MARK: - Properties
    
    var movies: [Movie] = []
    
    // MARK: - Constructors
    
    private init() {}
    
    // MARK: - Functions
    
    func loadAll(_ offset: Int, with context: NSManagedObjectContext) throws {
        let fetchRequestWithOffset: NSFetchRequest<Movie> = Movie.fetchRequest()
        let sortDescriptorByVoteAverage = NSSortDescriptor(key: "voteAverage", ascending: false)
        
        fetchRequestWithOffset.sortDescriptors = [sortDescriptorByVoteAverage]
        fetchRequestWithOffset.fetchLimit = 5
        fetchRequestWithOffset.fetchOffset = offset
        
        if try numberOfRecordsInDatabase(context) == 0 {
            try fetchMoviesAndSaveInDatabase(context: context)
        }
        
        movies = try context.fetch(fetchRequestWithOffset)
    }
    
    private func numberOfRecordsInDatabase(_ context: NSManagedObjectContext) throws -> Int {
        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
        
        return try context.fetch(fetchRequest).count
    }
    
    func find(id: Int64, context: NSManagedObjectContext) -> Movie? {
        let query: NSFetchRequest<Movie> = Movie.fetchRequest()
        let predicate = NSPredicate(format: "id = %i", id)
        
        query.predicate = predicate
        
        do {
            let movie = try context.fetch(query).first
            return movie
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    private func fetchMoviesAndSaveInDatabase(context: NSManagedObjectContext) throws {
        let idsToSearch: [Int] = fetchMoviesId()
        let movieDetails: [MovieDetailResponse] = fetchMovieDetails(idsToSearch: idsToSearch)
        
        for movieDetail in movieDetails {
            let movie = Movie(context: context)
            movie.id = Int64(movieDetail.id)
            movie.adult = movieDetail.adult
            movie.backdropPath = movieDetail.backdropPath
            movie.budget = movieDetail.budget as NSDecimalNumber
            movie.homepage = movieDetail.homepage
            movie.originalTitle = movieDetail.originalTitle
            movie.overview = movieDetail.overview
            movie.posterPath = movieDetail.posterPath
            movie.releaseDate = movieDetail.releaseDate
            movie.runtime = Int16(movieDetail.runtime)
            movie.status = movieDetail.status
            movie.tagline = movieDetail.tagline
            movie.title = movieDetail.title
            movie.voteAverage = movieDetail.voteAverage
            movie.voteCount = movieDetail.voteCount
            movie.cover = fetchCoverImage(backdropPath: movieDetail.backdropPath)
            
            for genreResponse in movieDetail.genres {
                let genre = GenreManager.shared.find(name: genreResponse.name, context: context)
                
                if let genre = genre {
                    movie.addToGenres(genre)
                }
            }
        }
        
        try saveChanges(context: context)
    }
    
    private func fetchMoviesId() -> [Int] {
        var ids: [Int] = []
        
        let groupMoviesSummary = DispatchGroup()
        groupMoviesSummary.enter()
        MovieService.loadMovies(onComplete: { (moviesResponse) in
            for movieSummary in moviesResponse.movieSummaries {
                ids.append(movieSummary.id)
            }
            
            groupMoviesSummary.leave()
        }) { (error) in
            print(error)
        }
        groupMoviesSummary.wait()
        
        return ids
    }
    
    private func fetchMovieDetails(idsToSearch: [Int]) -> [MovieDetailResponse] {
        var movieDetails: [MovieDetailResponse] = []
        
        let groupMovieDetails = DispatchGroup()
        for idToSearch in idsToSearch {
            groupMovieDetails.enter()
            
            MovieService.loadMovieDetails(movieId: idToSearch, onComplete: { (movieDetailResponse) in
                movieDetails.append(movieDetailResponse)
                
                groupMovieDetails.leave()
            }) { (error) in
                print(error)
            }
        }
        groupMovieDetails.wait()
        
        return movieDetails
    }
    
    private func fetchCoverImage(backdropPath: String) -> UIImage {
        let group = DispatchGroup()
        group.enter()
        
        let url = URL(string: MovieUtils.getImageInOriginalSize(for: backdropPath, size: .large))
        let data = try? Data(contentsOf: url!)
        
        group.leave()
        group.wait()
        
        return UIImage(data: data!)!
    }
    
    func saveChanges(context: NSManagedObjectContext) throws {
        try context.save()
    }
    
}
