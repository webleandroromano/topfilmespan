//
//  File.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation
import CoreData

class GenreManager {
    
    // MARK: - Singleton
    
    static let shared = GenreManager()
    
    // MARK: - Properties
    
    var genres: [Genre] = []
    
    // MARK: - Constructors
    
    private init() {}
    
    // MARK: - Functions
    
    func loadAll(with context: NSManagedObjectContext) throws {
        let fetchRequest: NSFetchRequest<Genre> = Genre.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if try context.fetch(fetchRequest).count == 0 {
            try fetchGenresAndSaveInDatabase(context: context)
        }
        
        genres = try context.fetch(fetchRequest)
    }
    
    func find(name: String, context: NSManagedObjectContext) -> Genre? {
        let query: NSFetchRequest<Genre> = Genre.fetchRequest()
        let predicate = NSPredicate(format: "name contains[c] %@", name)
        
        query.predicate = predicate
        
        do {
            let genre = try context.fetch(query).first
            return genre
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    private func fetchGenresAndSaveInDatabase(context: NSManagedObjectContext) throws {
        var genresToAdd: [GenreResponse] = []
        
        let group = DispatchGroup()
        
        group.enter()
        MovieService.loadGenres(onComplete: { (genresResponse) in
            for genreResponse in genresResponse.genres {
                genresToAdd.append(genreResponse)
            }
            
            group.leave()
        }) { (error) in
            print(error)
            fatalError()
        }
        group.wait()
        
        for genre in genresToAdd {
            let newGenre = Genre(context: context)
            newGenre.id = Int64(genre.id)
            newGenre.name = genre.name
        }
        
        try saveChanges(context: context)
    }
    
    func saveChanges(context: NSManagedObjectContext) throws {
        try context.save()
    }
    
}
