//
//  MovieViewModel.swift
//  TopFilmes
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation
import UIKit

struct MovieViewModel {
    
    let id: Int64
    let title: String
    let voteAverage: String
    let cover: UIImage
    var genres: String
    
    init(movie: Movie) {
        self.id = movie.id
        self.title = movie.title!
        self.voteAverage = "AVERAGE: \(movie.voteAverage)"
        self.genres = ""
        
        if let image = movie.cover as? UIImage {
            self.cover = image
        } else {
            self.cover = UIImage(named: "noCover")!
        }
        
        for genre in movie.genres! {
            if let genre = genre as? Genre {
                if self.genres.isEmpty {
                    self.genres.append(genre.name!)
                } else {
                    self.genres.append(", \(genre.name!)")
                }
            }
        }
    }
    
}
