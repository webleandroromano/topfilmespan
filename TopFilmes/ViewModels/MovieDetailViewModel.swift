//
//  MovieDetailViewModel.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation
import UIKit

struct MovieDetailViewModel {
    
    let id: Int64
    let title: String
    let voteAverage: String
    let voteCount: String
    let cover: UIImage
    let tagline: String
    let overview: String
    let runtime: String
    let status: String
    
    init(movie: Movie) {
        self.id = movie.id
        self.title = movie.title!
        self.voteAverage = "AVERAGE: \(movie.voteAverage)"
        self.voteCount = "Vote count: \(movie.voteCount) votes"
        self.tagline = movie.tagline!
        self.overview = "OVERVIEW:\n\n\(movie.overview!)"
        self.runtime = "Duration: \(movie.runtime) minutes"
        self.status = "Status: \(movie.status!)"
        
        if let image = movie.cover as? UIImage {
            self.cover = image
        } else {
            self.cover = UIImage(named: "noCover")!
        }
    }
    
}
