//
//  Alert.swift
//  TopFilmes
//
//  Created by Leandro Romano on 24/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation
import UIKit

struct Alert {
    
    static func show(on viewController: UIViewController, with title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.alertCancelContentButtonTitle, style: .cancel, handler: nil))
        
        viewController.present(alert, animated: true)
    }
    
}
