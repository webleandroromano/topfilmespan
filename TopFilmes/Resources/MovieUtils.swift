//
//  MovieUtils.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

struct MovieUtils {
    
    static func getImageInOriginalSize(for address: String, size: MovieCoverType) -> String {
        var sizePath = String()
        
        switch size {
        case .small:
            sizePath = "w500"
        default:
            sizePath = "original"
        }
        
        return "\(Constants.apiImageAddress)\(sizePath)\(address)"
    }
    
}
