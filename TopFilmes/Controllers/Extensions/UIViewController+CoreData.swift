//
//  UIViewController+CoreData.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import CoreData
import UIKit

extension UIViewController {
    
    var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
}
