//
//  MovieDetailsViewController.swift
//  TopFilmes
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lbVoteAverage: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbTagline: UILabel!
    @IBOutlet weak var lbOverview: UILabel!
    @IBOutlet weak var lbVoteCount: UILabel!
    @IBOutlet weak var lbRuntime: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    
    // MARK: - Properties
    
    var movieDetailViewModel: MovieDetailViewModel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Actions
    
    @IBAction func closeView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Functions
    
    private func setupComponents() {
        lbTitle.text = movieDetailViewModel.title
        lbVoteAverage.text = movieDetailViewModel.voteAverage
        lbTagline.text = movieDetailViewModel.tagline
        lbOverview.text = movieDetailViewModel.overview
        lbVoteCount.text = movieDetailViewModel.voteCount
        lbRuntime.text = movieDetailViewModel.runtime
        lbStatus.text = movieDetailViewModel.status
        
        ivCover.image = movieDetailViewModel.cover
    }

}
