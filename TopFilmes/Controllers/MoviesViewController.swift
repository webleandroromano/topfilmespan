//
//  MoviesViewController.swift
//  TopFilmes
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var cvMovies: UICollectionView!
    @IBOutlet weak var viLoading: UIView!
    @IBOutlet weak var aiLoading: UIActivityIndicatorView!
    
    // MARK: - Properties
    
    var movieViewModels: [MovieViewModel] = []
    
    var loadMoviesOffset: Int = 0
    
    var movieManager = MovieManager.shared
    
    var genreManager = GenreManager.shared
    
    var reachability: Reachability = Reachability.init()!
    
    lazy var moviesRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(fetchContentFromDatabase), for: .valueChanged)
        
        return refreshControl
    }()
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvMovies.delegate = self
        cvMovies.dataSource = self
        cvMovies.refreshControl = moviesRefreshControl
        cvMovies.allowsMultipleSelection = false
        
        performSelector(inBackground: #selector(fetchContentFromDatabase), with: nil)
        setupInternetConnectionObserver()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? MovieDetailsViewController else { return }

        let indexPathForSelectedCell = cvMovies.indexPathsForSelectedItems!.first
        let viewModel = movieViewModels[indexPathForSelectedCell!.row]
        
        guard let movie = movieManager.find(id: viewModel.id, context: context) else {
            print("error segue")
            return
        }
        
        vc.movieDetailViewModel = MovieDetailViewModel(movie: movie)
    }
    
    // MARK: - Functions
    
    @objc func fetchContentFromDatabase() {
        if userHasInternetConnection() {
            loadGenres()
            loadPopularMovies()
        }
    }
    
    private func loadGenres() {
        do {
            try genreManager.loadAll(with: context)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func loadPopularMovies() {
        do {
            try movieManager.loadAll(loadMoviesOffset, with: context)
            movieViewModels.append(contentsOf: movieManager.movies.map { return MovieViewModel(movie: $0) })
            
            DispatchQueue.main.async { [weak self] in
                self?.moviesRefreshControl.endRefreshing()
                self?.hideLoadingView()
                self?.cvMovies.reloadData()
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func showLoadingView() {
        viLoading.isHidden = false
        aiLoading.startAnimating()
    }
    
    func hideLoadingView() {
        viLoading.isHidden = true
        aiLoading.stopAnimating()
    }
    
    // MARK: - Internet Connection
    
    private func setupInternetConnectionObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(internetConnectionChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc private func internetConnectionChanged(note: Notification) {
        let reachabilityNote = note.object as! Reachability
        
        if reachabilityNote.connection == .none {
            Alert.show(on: self, with: Constants.alertNoInternetConnectionAvailableTitle, message: Constants.alertNoInternetConnectionAvailableMessage)
        }
    }
    
    private func userHasInternetConnection() -> Bool {
        if reachability.connection == .none {
            moviesRefreshControl.endRefreshing()
            Alert.show(on: self, with: Constants.alertNoInternetConnectionAvailableTitle, message: Constants.alertNoInternetConnectionAvailableMessage)
            return false
        }
        
        return true
    }
    
}
