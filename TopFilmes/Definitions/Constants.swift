//
//  Constants.swift
//  TopFilmes
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

struct Constants {
    
    // MARK: - Segues
    
    static let segueMoviesDetails = "movieDetailsSegue"
    
    // MARK: - API Basics
    
    static let apiKey = "c3bb4073b75df06bf126483634d2877c"
    
    static let apiKeyFormatted = "?api_key=\(apiKey)"
    
    static let apiBasePath = "https://api.themoviedb.org/3"
    
    // MARK: - API Endpoints
    
    static let apiListPopularMovies = "\(apiBasePath)/movie/popular\(apiKeyFormatted)&page=1"
    
    static let apiImageAddress = "https://image.tmdb.org/t/p/"
    
    static let apiListGenres = "\(apiBasePath)/genre/movie/list\(apiKeyFormatted)&language=en-US"
    
    static func apiListMovieDetails(movieId: Int) -> String {
        return "\(apiBasePath)/movie/\(movieId)\(apiKeyFormatted)&language=en-US"
    }
    
    // MARK: - Content
    
    static let alertCancelContentButtonTitle = "Close"
    
    static let alertNoInternetConnectionAvailableTitle = "Offline!"
    
    static let alertNoInternetConnectionAvailableMessage = "You are offline. Please, join to a Cellular or Wi-Fi connection."
    
}
