//
//  MovieService.swift
//  TopFilmes
//
//  Created by Leandro Romano on 21/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

class MovieService {
    
    private static let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = true
        config.httpAdditionalHeaders = ["Content-Type": "application/json"]
        config.timeoutIntervalForRequest = 30.0
        config.httpMaximumConnectionsPerHost = 5
        
        return config
    }()
    
    private static let session = URLSession(configuration: configuration)
    
    class func loadMovies(onComplete: @escaping (MoviesListResponse) -> Void, onError: @escaping (MovieError) -> Void) {
        guard let url = URL(string: Constants.apiListPopularMovies) else {
            onError(.invalidUrl)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil {
                
                guard let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    
                    guard let data = data else { return }
                    
                    do {
                        let moviesResponse = try JSONDecoder().decode(MoviesListResponse.self, from: data)
                        onComplete(moviesResponse)
                    } catch {
                        onError(.invalidResponsePayload)
                        print(error.localizedDescription)
                    }
                    
                } else {
                    onError(.serverError(statusCode: response.statusCode))
                    print("Error - Status Code \(response.statusCode)")
                }
                
            } else {
                onError(.taskError(error: error!))
                print(error!)
            }
        }
        
        dataTask.resume()
    }
    
    class func loadMovieDetails(movieId: Int, onComplete: @escaping (MovieDetailResponse) -> Void, onError: @escaping (MovieError) -> Void) {
        guard let url = URL(string: Constants.apiListMovieDetails(movieId: movieId)) else {
            onError(.invalidUrl)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil {
                
                guard let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    
                    guard let data = data else { return }
                    
                    do {
                        let movieDetails = try JSONDecoder().decode(MovieDetailResponse.self, from: data)
                        onComplete(movieDetails)
                    } catch {
                        onError(.invalidResponsePayload)
                        print(error.localizedDescription)
                    }
                    
                } else {
                    onError(.serverError(statusCode: response.statusCode))
                    print("Error - Status Code \(response.statusCode)")
                }
                
            } else {
                onError(.taskError(error: error!))
                print(error!)
            }
        }
        
        dataTask.resume()
    }
    
    class func loadGenres(onComplete: @escaping (GenresResponse) -> Void, onError: @escaping (MovieError) -> Void) {
        guard let url = URL(string: Constants.apiListGenres) else {
            onError(.invalidUrl)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if error == nil {
                
                guard let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    
                    guard let data = data else { return }
                    
                    do {
                        let movieDetails = try JSONDecoder().decode(GenresResponse.self, from: data)
                        onComplete(movieDetails)
                    } catch {
                        onError(.invalidResponsePayload)
                        print(error.localizedDescription)
                    }
                    
                } else {
                    onError(.serverError(statusCode: response.statusCode))
                    print("Error - Status Code \(response.statusCode)")
                }
                
            } else {
                onError(.taskError(error: error!))
                print(error!)
            }
        }
        
        dataTask.resume()
    }
    
}
