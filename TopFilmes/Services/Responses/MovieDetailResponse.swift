//
//  MovieDetailResponse.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

struct MovieDetailResponse: Codable {
    
    let id: Int
    let adult: Bool
    let backdropPath: String
    let budget: Decimal
    let homepage: String?
    let originalTitle: String
    let title: String
    let overview: String
    let posterPath: String
    let releaseDate: String
    let runtime: Int
    let status: String
    let tagline: String
    let voteAverage: Double
    let voteCount: Double
    let genres: [GenreResponse]
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case adult = "adult"
        case backdropPath = "backdrop_path"
        case budget = "budget"
        case homepage = "homepage"
        case originalTitle = "original_title"
        case title = "title"
        case overview = "overview"
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case runtime = "runtime"
        case status = "status"
        case tagline = "tagline"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case genres = "genres"
    }
    
}


