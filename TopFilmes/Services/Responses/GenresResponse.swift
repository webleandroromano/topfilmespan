//
//  GenresResponse.swift
//  TopFilmes
//
//  Created by Leandro Romano on 23/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

struct GenresResponse: Codable {
    
    let genres: [GenreResponse]
    
}

struct GenreResponse: Codable {
    
    let id: Int
    let name: String
    
}
