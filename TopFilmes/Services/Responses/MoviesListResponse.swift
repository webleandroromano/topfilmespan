//
//  MoviesListResponse.swift
//  TopFilmes
//
//  Created by Leandro Romano on 21/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

struct MoviesListResponse: Codable {
    
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let movieSummaries: [MovieSummary]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movieSummaries = "results"
    }
    
}
