//
//  MovieError.swift
//  TopFilmes
//
//  Created by Leandro Romano on 21/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

enum MovieError {
    
    case invalidUrl
    case emptyResponse
    case emptyData
    case invalidResponsePayload
    case taskError(error: Error)
    case serverError(statusCode: Int)
    
}
