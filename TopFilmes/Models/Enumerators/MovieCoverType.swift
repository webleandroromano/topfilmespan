//
//  MovieCoverType.swift
//  TopFilmes
//
//  Created by Leandro Romano on 21/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import Foundation

enum MovieCoverType {
    
    case large
    case small
    
}
