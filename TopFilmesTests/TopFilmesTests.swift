//
//  TopFilmesTests.swift
//  TopFilmesTests
//
//  Created by Leandro Romano on 19/03/19.
//  Copyright © 2019 Leandro Romano. All rights reserved.
//

import XCTest
import CoreData
@testable import TopFilmes

class TopFilmesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMovieViewModel() {
        let movie = Movie(context: NSManagedObjectContext(coder: NSCoder())!)
        movie.title = "Aquaman"
        movie.voteAverage = 6.8
        
        let viewModel = MovieViewModel(movie: movie)
        
        XCTAssertEqual(movie.title!, viewModel.title)
        XCTAssertEqual(viewModel.voteAverage, "AVERAGE: \(movie.voteAverage)")
    }
    
    func testMovieDetailViewModel() {
        let movie = Movie(context: NSManagedObjectContext(coder: NSCoder())!)
        movie.title = "Aquaman"
        movie.voteAverage = 6.8
        movie.voteCount = 4676
        movie.tagline = "Home Is Calling"
        movie.overview = "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne."
        movie.runtime = 144
        movie.status = "Released"
        
        let viewModel = MovieDetailViewModel(movie: movie)
        
        XCTAssertEqual(movie.title!, viewModel.title)
        XCTAssertEqual(viewModel.voteAverage, "AVERAGE: \(viewModel.voteAverage)")
        XCTAssertEqual(viewModel.voteCount, "Vote count: \(viewModel.voteCount) votes")
        XCTAssertEqual(movie.tagline!, viewModel.tagline)
        XCTAssertEqual(viewModel.overview, "OVERVIEW:\n\n\(viewModel.overview)")
        XCTAssertEqual(viewModel.runtime, "Duration: \(viewModel.runtime) minutes")
        XCTAssertEqual(viewModel.status, "Status: \(viewModel.status)")
    }

}
